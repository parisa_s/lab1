package oop.lab1;
import java.util.Scanner;

/**
 * Some tests of Person behavior.
 * @author jim
 */
public class Main {
	private static final Scanner console = new Scanner(System.in);
	/**
	 * Compare Person objects using their equals method.
	 * @param a first person to compare
	 * @param b second person to compare
	 */
	public static void testPersonEquals(Person a, Person b) {
		System.out.println("> Compare two Persons");
		System.out.println("Person a is " + a);
		System.out.println("Person b is " + b);
		boolean result = a.equals( b );
		System.out.println("a.equals(b) is " + result);
	}
	
	/**
	 * Compare any two objects using their equals method.
	 * @param a first object to compare
	 * @param b second object to compare
	 */
	public static void testObjectEquals(Object a, Object b) {
		System.out.println("> Compare two Objects");
		System.out.println("a is type " + a.getClass().getSimpleName() + " with value " + a);
		System.out.println("b is type " + a.getClass().getSimpleName() + " with value " + b);
		boolean result = a.equals( b );
		System.out.println("a.equals(b) is " + result);	
	}
	
	/** Wait for user to press ENTER. */
	public static void prompt(String message) {
		System.out.println();
		System.out.print(message);
		System.out.print(" [press enter]");
		console.nextLine();
	}
	
	public static void main(String[] args) {
		prompt("Compare two Persons that are definitely different");
		Person a = new Person("Cat");
		Person b = new Person("Mouse");
		testPersonEquals(a, b);
		testObjectEquals(a, b);
		
		prompt("Compare two Persons that should be same");
		a = new Person("Cat");
		b = new Person("Cat");
		testPersonEquals(a, b);
		testObjectEquals(a, b);
		
		prompt("Compare a Person and a Student");
		
		Person s = new Student("Cat", 1111);
		System.out.println("a is " + a);
		System.out.println("s is " + s);
		System.out.println("a.equals(s) is " + a.equals(s));
		System.out.println("s.equals(a) is " + s.equals(a));
		
	}
	
}
