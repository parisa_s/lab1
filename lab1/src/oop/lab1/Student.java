package oop.lab1;

//TODO Write class Javadoc
/**
 * A class of student that is a subclass of person class including name and id.
 * 
 * @author Parisa Supitayakul
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new student object.
	 * @param name is the name of the new student and id is the identifier of the student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	public boolean equals(Object other) {
		// (1) verify that obj is not null
		if (other == null) 
			return false;
		// (2) test if obj is the same class as "this" object
		if ( other.getClass() != this.getClass() )
			return false;
		// (3) cast obj to this class's type
		Student obj = (Student) other;
		// (4) compare whatever values determine "equals"
		if ( this.id == obj.id )
			return true;
		
		return false; 
	}
}
